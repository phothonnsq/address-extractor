# Address Extractor
## address extractor for manychat

# Technology
- AWS Lambda
- AWS API Gateway

# Refactor Code 
##move legacy code to new structure framework 

- https://github.com/awslabs/aws-serverless-express

Example
- https://github.com/awslabs/aws-serverless-express/tree/master/examples/basic-starter

# awscli

- ref: https://docs.aws.amazon.com/lambda/latest/dg/gettingstarted-awscli.html

# Zip file
- $ zip -r address-extractor.zip .
 
# Deployment 
- $ aws lambda update-function-code --function-name address-extractor --zip-file fileb://address-extractor.zip

