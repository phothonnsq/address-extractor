// Things to check
// tel, zipcode, province, amphoe, district
var fs = require('fs');
var stringSimilarity = require('string-similarity');

// var content = fs.readFileSync(__dirname + '/../constant/thai_address.json');
var content = fs.readFileSync(__dirname + '/../constant/flash_thai_address.json');
var th_address = eval('(' + content + ')');

var things_to_check = ['district', 'amphoe', 'province', 'zipcode'];
var provinces_synonym = {
  กรุงเทพ: [
    'กรุงเทพมหานครฯ',
    'กรุงเทพมหานคร',
    'กรุงเทพฯ',
    'กรุงเทพ',
    'กุงเทพ',
    'ก.ท.ม.',
    'ก.ท.ม',
    'กทม.',
    'กทม',
  ],
  พระนครศรีอยุธยา: ['อยุธยา'],
  อุบลราชธานี: ['อุบลฯ', 'อุบล'],
  ประจวบคีรีขันธ์: ['ประจวบฯ', 'ประจวบ'],
  สุราษฎร์ธานี: [
    'สุราษฏร์ธานี',
    'สุราษฎร์',
    'สุราษฏร์',
    'สุราษ',
    'สุราด',
    'สุราฎ',
    'สุราฏ'
  ],
  กาฬสินธุ์: ['กาฬสิน','กาลสิน'],
  สมุทรปราการ: ['สมุทปราการ','สมุทรปาการ','สมุดปราการ'],
  สมุทรสาคร: ['สมุทสาคร','สมุดสาคร'],
  สมุทรสงคราม: ['สมุทสงคราม','สมุดสงคราม'],
};
var amphoe_synonym = {
  "สุไหงโก-ลก": ["สุไหงโก-ลก","สุไหงโกลก","สุใหงโกลก","สุไหงโลก","สุไหงโกล"]
}
var strings_to_trim = [
  'จังหวัด',
  'จ.',
  'อำเภอ',
  'อ.',
  'ตำบล',
  'ต.',
  'รหัสไปรษณีย์',
  'รหัส',
  ',',
  '_',
  ' จ ',
  ' อ ',
  ' ต ',
  'เขต',
  'แขวง',
  'เเขวง',
  'เบอร์',
  'เบอ',
  'โทรศัพท์',
  'โทร',
  'โท'
];

// const regex_extract_tel = /\(?(\d{2,3})\)?[-. _~=]?(\d{3,4})[-. _~=]?([0-9]{4})/;
const regex_extract_tel_simple = /\d{9,10}/;
const regex_extract_tel = /\(?(0[689][0-9]{1,2})\)?[-. _~=]?(\d{3,4})[-. _~=]?([0-9]{4})/;
const regex_extract_zipcode = /(?<!\d)\d{5}(?!\d)/;
const regex_tel_zip_no_space = /\d{15}/;

function isMightHave(word_to_search, words_to_be_searched) {
  var matches = stringSimilarity.compareTwoStrings(
    word_to_search + '',
    words_to_be_searched + '' //make sure to be string
  );
  console.log(
    `word_to_search: ${words_to_be_searched} matches score ${matches}`
  );
  return matches > 0.2;
}

function similarityOf(word_to_search, words_to_be_searched) {
  return stringSimilarity.compareTwoStrings(
    word_to_search + '',
    words_to_be_searched + '' //make sure to be string
  );
}

function findOtherDetail(matchdata, input_address) {
  // console.log('matchdata', matchdata);
  // console.log('input_address', input_address);

  //old logic : clean matched data
  //ที่อยู่ที่ตัดจังหวัด อำเภอ บลา ๆ ออกไป
  // things_to_check.map(function(x) {
  //   input_address = input_address.replace(matchdata[x], '');
  //   if (
  //     x === 'province' &&
  //     provinces_synonym[matchdata.province] !== undefined
  //   ) {
  //     //กรณีเป็น synonym
  //     var the_province_synonyms = provinces_synonym[matchdata.province];
  //     the_province_synonyms.map(function(synonym) {
  //       input_address = input_address.replace(synonym, '');
  //     });
  //   }
  // });

  //clean zipcode & tel
  // [regex_extract_tel, regex_extract_zipcode].map(regex => {
  //   const is_valid = regex.test(input_address);
  //   if (is_valid) {
  //     input_address = input_address.replace(input_address.match(regex)[0], '');
  //   }
  // });

  //new logic
  //substring until district or amphoe
  const { district, amphoe } = matchdata;
  const district_index = input_address.indexOf(district);
  if (district_index > 0) {
    //found correct district -> assume other details come before district
    //substring until disttrict
    input_address = input_address.substring(0, district_index);
  } else {
    //found correct amphoe instead
    input_address = input_address.substring(0, input_address.indexOf(amphoe));
  }

  //คลีนคำบอกต่าง ๆ
  strings_to_trim.map(function(x) {
    input_address = input_address.replace(x, '');
  });

  return input_address.trim();
}

function switch_chada_and_patak(word_to_switch) {
  if(typeof word_to_switch !== "string") return;
  if(word_to_switch.indexOf('ฎ') > 0) {
    return word_to_switch.replace('ฎ','ฏ');
  } else if (word_to_switch.indexOf('ฏ') > 0) {
    return word_to_switch.replace('ฏ','ฎ');
  } else {
    return;
  }
}

function found_when_swith_chada_and_patak(add_ref, input_address) {
  if(typeof add_ref !== "string") return false;
  if (add_ref.indexOf('ฎ') > 0 || add_ref.indexOf('ฏ') > 0) {
    const switched_ref = switch_chada_and_patak(add_ref);
    return input_address.search(switched_ref) > 0;
  }
}

function remove_tone(word_to_remove) { //เอาวรรณยุกต์ออกหมด
  if(typeof word_to_remove !== "string") return;
  return word_to_remove.replace(/[่้๊๋]/g,'');
}

function found_when_remove_tone(word_to_find,address) {
  if(typeof word_to_find !== "string") return false;
  return remove_tone(address).indexOf(remove_tone(word_to_find)) > 0;
}

function switch_sara_a_ae(word_to_switch) { //กรณีใช้ เเ แทน แ
  if(typeof word_to_switch !== "string") return;
  if(word_to_switch.indexOf('เเ') > 0) {
    return word_to_switch.replace('เเ','แ');
  }
  return;
}

function found_when_change_sara_a_ae(word_to_find, address) {
  if(typeof word_to_find !== "string") return false;
  return address.indexOf(switch_sara_a_ae(word_to_find)) > 0;
}

function found_when_change_some_char(word_to_find,address) {
  if(typeof word_to_find !== "string") return false;
  return  found_when_remove_tone(word_to_find,address) || 
          found_when_swith_chada_and_patak(word_to_find,address) || 
          found_when_change_sara_a_ae(word_to_find,address);
}


// focus only zipcode, province, amphoe, district
function checkAddressValidity(input_address) {
  if (input_address === undefined) return;
  //เจอข้อมูลตรงกับอันไหนบ้าง
  var all_matches = []; //เก็บที่เจอทุกอย่าง
  var some_matches = []; //เก็บที่เจอบางอย่าง (หายไป 1 อย่าง)

  //คลีนคำบอกต่าง ๆ
  // strings_to_trim.map(function(x) {
  //   input_address = input_address.replace(x, '');
  // });

  //คลีน zero-width characters
  input_address = input_address.replace(/[\u200B-\u200D\uFEFF]/g, '');
  //ลองหา
  for (i in th_address) {
    var this_add_ref = th_address[i];
    var found = {};

    //province
    found.province = input_address.search(this_add_ref.province) > 0;
    if(!found.province) {
      const province_ref = this_add_ref.province;
      //ลองสลับ ฏ​ ฎ ดู
      if(found_when_swith_chada_and_patak(province_ref,input_address)) found.province = true;

      //ถ้าเป็นจังหวัดที่คนชอบย่อ
      if (provinces_synonym[province_ref] !== undefined) {
        var synonyms = provinces_synonym[province_ref];
        var found_synonym = 0; //นับว่าเจอมั่งปะ
        for (synonym of synonyms) {
          if (input_address.search(synonym) > 0) found_synonym++;
        }
        if (found_synonym) {
          found.province = true;
        }
      }
    }
    
    //amphoe
    const address_wo_province = input_address.replace(
      new RegExp(this_add_ref.province + '(?!.*' + this_add_ref.province +')'),
      ''
    );
    
    found.amphoe = address_wo_province.search(this_add_ref.amphoe) > 0;
    if(!found.amphoe) {
      const amphoe_ref = this_add_ref.amphoe;

      //กรณีอ.เมือง
      if(amphoe_ref === "เมือง" + this_add_ref.province) {
        if(address_wo_province.indexOf("เมือง") > 0) found.amphoe = true;
      }

      //ลองสลับ ฏ​ ฎ ดู
      if(found_when_swith_chada_and_patak(amphoe_ref,address_wo_province)) found.amphoe = true;

      //กรณีเป็นอำเภอที่ชอบพิมพ์ผิด
      if(amphoe_synonym[amphoe_ref] !== undefined) {
        var synonyms = amphoe_synonym[amphoe_ref];
        var found_synonym = 0; //นับว่าเจอมั่งปะ
        for (synonym of synonyms) {
          if (address_wo_province.search(synonym) > 0) found_synonym++;
        }
        if (found_synonym) {
          found.amphoe = true;
        }
      }
    }
    

    //district
    const address_wo_pro_amp = address_wo_province.replace(
      new RegExp(this_add_ref.amphoe + '(?!.*' + this_add_ref.amphoe +')'),
      ''
    );   
    found.district = address_wo_pro_amp.search(this_add_ref.district) > 0;
    //ลองสลับ ฏ​ ฎ ดู
    if(found_when_swith_chada_and_patak(this_add_ref.district,address_wo_pro_amp)) found.district = true;

    //zipcode
    const address_wo_pro_amp_dis = address_wo_pro_amp.replace(
      new RegExp(new RegExp(this_add_ref.district + '(?!.*' + this_add_ref.district +')'),),
      ''
    );
    found.zipcode =
      address_wo_pro_amp_dis.search(this_add_ref.zipcode + '') > 0;

    var found_count = Object.values(found).filter(v => v).length; //เจอกี่อย่าง
    if (found_count === Object.keys(found).length) {
      //หาเจอทุกอย่าง
      //ที่อยู่ที่ตัดจังหวัด อำเภอ บลา ๆ ออกไป
      this_add_ref.other_detail = findOtherDetail(this_add_ref, input_address);
      all_matches.push(this_add_ref);
    } else if (found_count === Object.keys(found).length - 1) {
      //ข้อมูลหาย 1 อย่าง
      var some_match_data = {
        suspect_address: this_add_ref,
        ...found
      };
      some_matches.push(some_match_data);
    } 
    //กรณีหายเกิน 1 อย่างก็ถือว่าไม่เจอไปเลย
  }

  //ถ้าเจอแค่อันเดียวก็โอเคเลย
  if (all_matches.length === 1) {
    console.log('เจอครบแบบเดียว');
    return {
      province_valid: true,
      amphoe_valid: true,
      district_valid: true,
      zipcode_valid: true,
      data: all_matches[0]
    };
  }

  //ถ้าเกิน เช็คกลับ
  if (all_matches.length > 1) {
    // เกิน 1 จะเช็คกลับทีละอัน พอเช็คเสร็จลบที่เช็คแล้ว
    console.log('เจอครบเกิน 1 แบบ');
    console.log('all_matches',all_matches)

    //เรียง all_matches ให้ตัวที่มชื่อตำบล+อำเภอยาวที่สุดมาก่อน สำหรับกรณีเช่น มีทั้งต.สะเตง และ สะเตงนอก ให้หาสะเตงนอกก่อน
    all_matches.sort(function(a, b) {
      return (b.district + b.amphoe).length - (a.district + a.amphoe).length;
    });

    var recheck_result = all_matches.map(a_match => {

      // ถ้าเช็คกลับแล้วมีหมดคือ ok return อันนั้นเลย
      var temp_add = input_address;
      var ok_to_return = true;
      things_to_check.map(key => {
        console.log('temp_add',temp_add);
        if (temp_add.search(a_match[key]) > 0) {
          temp_add = temp_add.replace(a_match[key], ''); //อันไหนหาแล้วตัดออก จะได้ไม่หาซ้ำกัน
        } else if (
          key === 'province' &&
          provinces_synonym[a_match[key]] !== undefined
        ) {
          //กรณีเป็นแบบ synonym ลองวนดูว่าเจอบ้างมั้ย ถ้าเจอ ก็แทนค่า ถ้าไม่เจอก็ ok_to_return = false
          var synonyms = provinces_synonym[a_match[key]];
          var found_synonym = 0; //นับว่าเจอมั่งปะ
          var the_synonym_found = '';
          for (synonym of synonyms) {
            if (input_address.search(synonym) > 0) {
              found_synonym++;
              the_synonym_found = synonym;
            }
          }
          if (found_synonym) {
            temp_add = temp_add.replace(the_synonym_found, '');
          } else {
            ok_to_return = false;
          }
        } else if (
          key === 'amphoe'
        ) {
          if(amphoe_synonym[a_match[key]] !== undefined) {
            //กรณีเป็นแบบ synonym ลองวนดูว่าเจอบ้างมั้ย ถ้าเจอ ก็แทนค่า ถ้าไม่เจอก็ ok_to_return = false
            var synonyms = amphoe_synonym[a_match[key]];
            var found_synonym = 0; //นับว่าเจอมั่งปะ
            var the_synonym_found = '';
            for (synonym of synonyms) {
              if (input_address.search(synonym) > 0) {
                found_synonym++;
                the_synonym_found = synonym;
              }
            }
            if (found_synonym) {
              temp_add = temp_add.replace(the_synonym_found, '');
            } else {
              ok_to_return = false;
            }
          }

          if(a_match[key] === "เมือง" + a_match["province"]) {
            //กรณีอ.เมือง
            console.log('อ.เมือง')
            if(temp_add.indexOf("เมือง") > 0) {
              temp_add = temp_add.replace("เมือง", '');
            } else {
              ok_to_return = false;
            }
          }

        } else if (a_match[key].indexOf('ฎ') > 0 || a_match[key].indexOf('ฏ') > 0) { //กรณีสลับ ฎ​-ฏ
          if(found_when_swith_chada_and_patak(a_match[key], temp_add)) {
            temp_add = temp_add.replace(switch_chada_and_patak(a_match[key]), '');
          } else {
            ok_to_return = false;
          }
        } else {
          ok_to_return = false;
        }
      });
      return ok_to_return;
    });
    var result_index = recheck_result.indexOf(true);
    if (result_index < 0) {
      // เช็คกลับไม่ผ่านทั้งหมด ถือว่าไม่เจอ
      return {
        province_valid: false,
        amphoe_valid: false,
        district_valid: false,
        zipcode_valid: false,
        data: {}
      };
    }
    var result_data = all_matches[result_index];
    result_data.other_detail = findOtherDetail(
      all_matches[result_index],
      input_address
    );
    return {
      province_valid: true,
      amphoe_valid: true,
      district_valid: true,
      zipcode_valid: true,
      data: result_data
    };
  }

  //ถ้าไม่เจอที่ครบเลย แต่เจอเกือบครบ (ขาด 1 อย่าง)
  //เจออันที่น่าสงสัยอันนึง เอาค่าที่เหลือมาเติมเองเลย
  if (some_matches.length === 1) {
    console.log('ข้อมูลหาย 1 อย่าง และเจอที่ใกล้เคียง 1 แบบ เติมให้เลย');
    var matched = some_matches[0].suspect_address;
    matched.other_detail = findOtherDetail(matched, input_address);
    //เอาข้อมูลจริงไปใส่ข้อมูลเลขที่บ้านเพิ่มเข้าไป
    // some_matches[0].suspect_address.other_detail = findOtherDetail(
    //   input_address
    // );
    return {
      province_valid: true,
      amphoe_valid: true,
      district_valid: true,
      zipcode_valid: true,
      data: matched
    };
  }

  if (some_matches.length > 1) {
    console.log('ข้อมูลหาย 1 อย่าง และเจอที่ใกล้เคียง > 1 แบบ');
    console.log('some_matches',some_matches);

    //วนกลับเพื่อเช็คว่าเป็นอันไหน
    var guessing_address = [];
    for (a_match of some_matches) {
      //ข้อมูลที่เจอแล้วมีอะไรบ้าง
      var thingsRemainingToCheck = things_to_check.filter(v => a_match[v]);
      var temp_address = input_address;
      var ok_to_return = true;
      for (thingToCheck of thingsRemainingToCheck) {
        var wordToCheck = a_match['suspect_address'][thingToCheck];

        
        if (
          thingToCheck === 'province' && //ถ้าเป็นจังหวัดที่ชอบใช้คำคล้าย
          provinces_synonym[wordToCheck] !== undefined
        ) {
          const synonyms = provinces_synonym[wordToCheck];
          for (synonym of synonyms) {
            if (temp_address.search(synonym) > 0) found_synonym++;
          }
          if (found_synonym.length === 0) {
            ok_to_return = false;
          }
        } else if ( 
          thingToCheck === 'amphoe' 
        ) {
          //ถ้าเป็นอำเภอที่ชอบใช้คำคล้าย
          if(amphoe_synonym[wordToCheck] !== undefined) {
            const synonyms = amphoe_synonym[wordToCheck];
            for (synonym of synonyms) {
              if (temp_address.search(synonym) > 0) found_synonym++;
            }
            if (found_synonym.length === 0) {
              ok_to_return = false;
            }
          }

          //ถ้าเป็นอ.เมือง
          if(wordToCheck === "เมือง" + a_match['suspect_address']['province']) {
            if(temp_address.indexOf("เมือง") > 0) {
              temp_address = temp_address.replace("เมือง");
            } else {
              ok_to_return = false;
            }
          }
          
        } else {
          if (temp_address.search(wordToCheck) > 0) {
            temp_address = temp_address.replace(wordToCheck);
          } else {
            ok_to_return = false;
          }
        }
      }
      if (ok_to_return) guessing_address.push(a_match);
    }

    //ถ้าเจอแบบเดียว จบ เอาอันนั้นเลย
    if (guessing_address.length === 1) {
      // console.log('เช็คกลับแล้วเหลืออันเดียว');
      var matched = guessing_address[0].suspect_address;
      matched.other_detail = findOtherDetail(matched, input_address);
      return {
        province_valid: true,
        amphoe_valid: true,
        district_valid: true,
        zipcode_valid: true,
        data: matched
      };
    }

    //ถ้าเจอหลายแบบ
    if (guessing_address.length > 1) {
      console.log('guessing_address',guessing_address);
      //มีแบบเดียวที่ zipcode หาย เอาอันนั้น (น่าจะพิมพ์โค้ดผิด)
      var wrong_zipcode_addresses = guessing_address.filter(a => !a.zipcode);
      if (wrong_zipcode_addresses.length === 1) {
        var matched = wrong_zipcode_addresses[0].suspect_address;
        matched.other_detail = findOtherDetail(matched, input_address);
        return {
          province_valid: true,
          amphoe_valid: true,
          district_valid: true,
          zipcode_valid: true,
          data: matched
        };
      }

      //ยังไม่เจออีก อาจจะพิมพ์ผิดไปอย่างนึง ลองเช็คสิ่งที่หายไปด้วย fuzzy search
      for (a_address of guessing_address) {
        console.log('use fuzzy search');
        // อะไรขาดไป
        // console.log('input_address', input_address);
        const missing_things = things_to_check.filter(v => !a_address[v]);
        // console.log('missing_thing', missing_things[0]);
        const missing_thing = missing_things[0];

        //ตัดคำที่เจอแล้วออกก่อน
        let remaining_address = input_address;
        const suspect_address = a_address.suspect_address;
        Object.values(suspect_address).map(
          x => (remaining_address = remaining_address.replace(x, ''))
        );
        //ลองหากลับแบบคร่าว ๆ ถ้าเจอก็คืนค่าเลย
        // console.log(
        //   'suspect_address[missing_thing]',
        //   suspect_address[missing_thing]
        // );
        let found = isMightHave(
          remaining_address,
          suspect_address[missing_thing]
        ) || found_when_change_some_char(suspect_address[missing_thing], remaining_address); //ลองหาแบบไม่สนใจวรรณยุกต์หริอ ฏ ฎ, สระเอสองตัวแทนแอ
        
        
        console.log('found', found);
        if (found) {
          var matched = suspect_address;
          matched.other_detail = findOtherDetail(matched, input_address);
          return {
            province_valid: true,
            amphoe_valid: true,
            district_valid: true,
            zipcode_valid: true,
            data: matched
          };
        }
      }

      //เพราะจะมีตำบลที่ชื่อซ้อนกันเยอะ เช่น เวียง รอบเวียง ก็เช็ครอบเวียงก่อนเลย
      //เรียงเอาที่ตำบลยาวสุดมาก่อน
      //อันนี้ถือตำบลมีชื่อซ้อนด้านในเลยเจอหลายอัน ให้เช็คตัวที่ตำบลยาวที่สุดก่อน ถ้าใช่เอาเลย
      // let we_can_check_by_checking_longest_district_first = true;
      //ถ้าอื่น ๆ ไม่เหมือนกัน ไม่ต้องเช็ควิธีนี้
      // console.log(
      //   'we_can_check_by_checking_longest_district_first',
      //   we_can_check_by_checking_longest_district_first
      // );
      // for (a_address of guessing_address) {
      //   ['amphoe', 'province', 'zipcode'].map(thing => {
      //     if (
      //       guessing_address[0].suspect_address[thing] !==
      //       a_address.suspect_address[thing]
      //     )
      //       we_can_check_by_checking_longest_district_first = false;
      //   });
      // }

      // if (we_can_check_by_checking_longest_district_first) {
      //   guessing_address = guessing_address.sort((a, b) => {
      //     a.suspect_address.district.length - b.suspect_address.district.length;
      //   });
      //   for (a_address of guessing_address) {
      //     console.log('a_address', a_address);
      //     const existed_things = things_to_check.filter(v => a_address[v]);
      //     let ok_to_return = true;
      //     let address_to_check = input_address;

      //     existed_things.map(things => {
      //       const { suspect_address } = a_address;
      //       const word_to_search = suspect_address[things];
      //       if (address_to_check.indexOf(word_to_search) > 0) {
      //         console.log(`found ${word_to_search} in ${address_to_check}`);
      //         address_to_check = address_to_check.replace(word_to_search, '');
      //       } else {
      //         ok_to_return = false;
      //       }
      //     });

      //     if (ok_to_return) {
      //       let matched = a_address.suspect_address;
      //       matched.other_detail = findOtherDetail(matched, input_address);
      //       return {
      //         province_valid: true,
      //         amphoe_valid: true,
      //         district_valid: true,
      //         zipcode_valid: true,
      //         data: matched
      //       };
      //     }
      //   }
      // }
    }
  }

  //ถ้าไม่มีเลย
  console.log('ไม่เจอ');
  return {
    province_valid: false,
    amphoe_valid: false,
    district_valid: false,
    zipcode_valid: false,
    data: {}
  };
}

function is_tel_and_address_valid(input_address) {
  const {
    province_valid,
    amphoe_valid,
    district_valid,
    zipcode_valid,
    data
  } = checkAddressValidity(input_address);

  let tel_valid =
    regex_extract_tel.test(input_address) ||
    regex_extract_tel_simple.test(input_address);

  //case zip_tel ติดกันเลย
  if (regex_tel_zip_no_space.test(input_address)) {
    console.log('เจอ zip ติดเบอร์');
    tel_valid = true;
  }

  if (zipcode_valid) {
    //ไม่ต้องหาใหม่
    return {
      province_valid,
      amphoe_valid,
      district_valid,
      zipcode_valid,
      tel_valid
    };
  } else {
    //หา zipcode ใหม่ เผื่อมี

    const zipcode_valid_by_regex = regex_extract_zipcode.test(input_address);
    return {
      province_valid,
      amphoe_valid,
      district_valid,
      zipcode_valid: zipcode_valid_by_regex,
      tel_valid
    };
  }
}

//should be used only after validation
function return_address_detail(input_address) {
  const { data } = checkAddressValidity(input_address);
  return data;
}

//should be used only after validation
function return_tel_detail(input_address) {
  //case zip_tel ติดกันเลย /\d{15}/;
  if (regex_tel_zip_no_space.test(input_address)) {
    const zip_and_tel = input_address.match(regex_tel_zip_no_space)[0];
    return zip_and_tel.substring(5, 15);
  }

  //clean zipcode
  let zipcode = input_address.match(regex_extract_zipcode);
  if(!zipcode) { //ถ้าหาไม่เจอ ลองดึงค่าจากตัวหาที่อยู่
    zipcode = return_address_detail(input_address).zipcode;
  }
  input_address = input_address.replace(zipcode,"");

  const tel_found = input_address.match(regex_extract_tel);
  let tel_from_regex, cleaned_tel;
  if(tel_found) {
    tel_from_regex = tel_found[0]
    cleaned_tel = tel_from_regex.match(/\d/g).join('');
    return cleaned_tel;
  }
}

module.exports = {
  get_address_valid_and_detail: checkAddressValidity,
  is_tel_and_address_valid,
  return_address_detail,
  return_tel_detail
};
