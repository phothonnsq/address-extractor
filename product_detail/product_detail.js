const axios = require('axios');

const product_detail_endpoint = 'https://api.sheety.co/4af84282-bef4-40f0-9462-ac57651ecb10';

//this function return promise
function get_product_detail(sku_id, field) {
    return axios
            .get(product_detail_endpoint)
            .then(product_detail_res => {
                const product_detail_array = product_detail_res.data;
                for(product_detail of product_detail_array) {
                    if(product_detail['sKU-ID'] === sku_id)  return product_detail[field];
                }
            });
}

module.exports = { get_product_detail };