// index.js
const path = require('path');
const express = require("express");
const bodyParser = require('body-parser');
const cors = require('cors');
const compression = require('compression');
const app = express();
const router = express.Router();

router.use(compression());
router.use(cors());
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));

const {
    is_tel_and_address_valid,
    return_address_detail,
    return_tel_detail,
} = require("./address/check_validity");

const { get_product_detail } = require("./product_detail/product_detail");

router.get("/", (req, res) => {
    res.send(validation_res);
    res.send(
        '<p>This is an API for NSQ chatbot.</p> <p>please read the document <a href="https://docs.google.com/presentation/d/1e63kJ12-Jfze61679QVosP7WxJZrYlu4Nl4fs4hZgtU/edit?usp=sharing">here</a> </p> please contact chayatan.ang@gmail.com if you want to use'
    );
});

//check if address is valid
router.post("/address/is_valid", (req, res) => {
    let input_address = req.body.address
    let validation_res = is_tel_and_address_valid(input_address);
    res.send(validation_res);
});

//use only when data is valid
//return data: province, district, amphoe, zipcode
router.post("/address/get_address_data_from_address", (req, res) => {
    let input_address = req.body.address;
    let address_data = return_address_detail(input_address);
    res.send(address_data);
});

router.post("/address/get_tel_from_address", (req, res) => {
    let input_address = req.body.address;
    console.log("input_address", input_address);
    let tel_no = return_tel_detail(input_address);
    console.log("tel_no", tel_no);
    res.send({ tel_no: tel_no });
});

//get product detail from sku id
router.post("/product_detail/get_from_sku_id", (req, res) => {
    let sku_id = req.body.sku_id;
    let field = req.body.field;
    get_product_detail(sku_id, field).then((query_result) =>
        res.send({ result: query_result })
    );
});

app.use('/', router)

app.listen(3000, () => console.log(`Listening on: 3000`));
// module.exports.handler = serverless(app);
