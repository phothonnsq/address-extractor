// สำหรับเปลี่ยน อ.เมืองกระบี่ -> อ.เมือง
var fs = require('fs');
console.log('\n *START* \n');
var content = fs.readFileSync('original_thai_address.json');
var obj = eval('(' + content + ')');

console.log('Output Content : \n' + obj[0]['province']);

var newAddArray = [];

for (i in obj) {
  var thisAdd = obj[i];
  if (obj[i].amphoe === 'เมือง' + obj[i].province) {
    thisAdd.amphoe = 'เมือง';
    // console.log(thisAdd);
  }
  newAddArray.push(thisAdd);
}

fs.writeFile('thai_address.json', JSON.stringify(newAddArray), err => {
  if (err) console.log(err);
  console.log('Successfully Written to File.');
});
